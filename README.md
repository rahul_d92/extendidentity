## Sample Project which portrays IdentityUser extension for Kontiki

### Instructions to Run 

1.      Update database connection

2.      Run database update



### Files Altered or created 

1.     ``` _LoginPartial.cshtml```

2.      ```ApplicationUser.cs```

3.      ```Branch.cs```

4.     ```DummyData.cs (WIP)``

5.      ```StartUp.cs```

6.      ```ApplicationDbContext.cs```


####NOTE THE BELOW LINE OF CODE IN StartUp.cs line 43

```  services.AddDefaultIdentity<ApplicationUser>(
                    options => options.Stores.MaxLengthForKeys = 128)
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultUI(UIFramework.Bootstrap4)
                .AddDefaultTokenProviders();```