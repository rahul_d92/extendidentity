﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace ExtendIdentity.Models
{
    public class ApplicationUser: IdentityUser
    {
        public ApplicationUser(): base(){}

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int BranchId { get; set; }
        public Branch Branch { get; set; }

    }
}
