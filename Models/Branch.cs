﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Threading.Tasks;

namespace ExtendIdentity.Models
{
    public class Branch
    {
        public int BranchId { get; set; }
        public string BranchName { get; set; }
    }
}
