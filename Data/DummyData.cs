﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExtendIdentity.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace ExtendIdentity.Data
{
    public class DummyData
    {
        public static async Task Initialize(ApplicationDbContext context,UserManager<ApplicationUser> userManager)
        {
            context.Database.EnsureCreated();

            string role1 = "Admin";
            string role2 = "Finance";
            string role3 = "Front-Desk";

            var roleManager = new RoleStore<IdentityRole>(context);

            if (roleManager.FindByNameAsync(role1) == null)
            {
                await roleManager.CreateAsync(new IdentityRole(role1));
            }

            if (roleManager.FindByNameAsync(role2) == null)
            {
                await roleManager.CreateAsync(new IdentityRole(role2));
            }

            if (roleManager.FindByNameAsync(role3) == null)
            {
                await roleManager.CreateAsync(new IdentityRole(role3));
            }

            string username1 = "superadmin@admin.com";
            string password1 = "@f6F-75BD5[U";

            if (userManager.FindByNameAsync(username1) == null)
            {
                var user = new ApplicationUser
                {
                    UserName = username1,
                    Email = username1,
                    FirstName = "Super",
                    LastName = "Admin"
                };

                var result = await userManager.CreateAsync(user);

                if (result.Succeeded)
                {
                    await userManager.AddPasswordAsync(user, password1);
                    await userManager.AddToRoleAsync(user, role1);
                }

                var adminId1 = user.Id;
            }
            string username2 = "finance@admin.com";
            string password2 = "@f6F-75BD5[U";

            if (userManager.FindByNameAsync(username2) == null)
            {
                var user = new ApplicationUser
                {
                    UserName = username2,
                    Email = username2,
                    FirstName = "Finance",
                    LastName = "Staff"
                };

                var result = await userManager.CreateAsync(user);

                if (result.Succeeded)
                {
                    await userManager.AddPasswordAsync(user, password2);
                    await userManager.AddToRoleAsync(user, role2);
                }

                var financeId = user.Id;
            }
            string username3 = "frontoffice@admin.com";
            string password3 = "@f6F-75BD5[U";

            if (userManager.FindByNameAsync(username3) == null)
            {
                var user = new ApplicationUser
                {
                    UserName = username3,
                    Email = username3,
                    FirstName = "Front",
                    LastName = "Desk"
                };

                var result = await userManager.CreateAsync(user);

                if (result.Succeeded)
                {
                    await userManager.AddPasswordAsync(user, password3);
                    await userManager.AddToRoleAsync(user, role3);
                }

                var frontdeskId = user.Id;
            }

            context.SaveChangesAsync().Wait();

        }
    }
}
